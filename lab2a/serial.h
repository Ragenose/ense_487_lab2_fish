#include "registers.h"
#include "led.h"
/**
  *  FILENAME serial.h
  *
  *  DESCRIPTION:
  *
  * AUTHOR: Trevor Douglas SID
*/
extern Reg32 regRCC_APB1ENR;
extern Reg32 regRCC_APB2ENR;
extern Reg32 regGPIOA_CRL; 
extern Reg32 regUSART2_CR1;
extern Reg32 regUSART2_BRR;
extern Reg32 regUSART2_SR;
extern Reg32 regUSART2_DR;
extern Reg32 regNVIC_ISER1;

//public functions available for use
void open_serial(void);
void close_serial(void);

void sendByte(uint8_t);
uint8_t getByte();
int getkey(int *c);

void sendString(uint8_t [], int size);
