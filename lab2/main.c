#include <stdio.h>
#include "main.h"




int main()
{
	setupRegs();
	open_led();
  open_serial();
  //Writing to the following will turn off a light.
	uint8_t data;
	while(1){
		data = getByte();
		while(data != 0x0D){
			if(data != 0x7F){
				inBuffer[inBufferCounter]=data;
				sendByte(data);
				inBufferCounter++;
				data = getByte();
			}
			else{
				sendByte(data);
				if(inBufferCounter != 0){
					inBufferCounter--;
				}
				data = getByte();
			}
		}
		sendByte('\r');
		sendByte('\n');
		
		run();
		inBufferCounter = 0;
		sendByte('\r');
		sendByte('\n');
	}
	return 0;
}


