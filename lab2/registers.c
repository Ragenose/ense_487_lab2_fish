#include "util.h"
#include "registers.h"

Reg32 regRCC_APB1ENR;
Reg32 regRCC_APB2ENR;
Reg32 regRCC_CFGR;

Reg32 regGPIOA_CRL; 
Reg32 regGPIOA_BRR; 
Reg32 regGPIOA_BSRR; 

Reg32 regGPIOB_ODR; 
Reg32 regGPIOB_CRH; 
Reg32 regGPIOB_BSRR; 
Reg32 regGPIOB_BRR; 


Reg32 regUSART2_BRR;
Reg32 regUSART2_SR;
Reg32 regUSART2_DR;
Reg32 regUSART2_CR1;
Reg32 regUSART2_CR2;
Reg32 regUSART2_CR3; 

void setupRegs(void){
	regRCC_APB1ENR 	= (Reg32)RCC_APB1ENR;
	regRCC_APB2ENR 	= (Reg32)RCC_APB2ENR;
	
	regRCC_CFGR = (Reg32)RCC_CFGR;
	
	regGPIOA_CRL 		= (Reg32)GPIOA_CRL;
	
  regGPIOB_ODR 		= (Reg32)GPIOB_ODR; 
	regGPIOB_CRH 		= (Reg32)GPIOB_CRH; 
	regGPIOB_BSRR 	= (Reg32)GPIOB_BSRR; 
  regGPIOB_BRR 		= (Reg32)GPIOB_BRR; 
	
	regUSART2_BRR 	= (Reg32)USART2_BRR;
	regUSART2_SR 		= (Reg32)USART2_SR;
	regUSART2_DR 		= (Reg32)USART2_DR;
	regUSART2_CR1		=	(Reg32)USART2_CR1;
	
}

